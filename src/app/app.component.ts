import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Currency } from './models/currency';
import { AmountChangeAction } from './actions/amount';
import { Store } from '@ngrx/store';
import * as fromRoot from './reducers';
import { Observable } from 'rxjs';
import { CurrenciesUpdateAction } from './actions/currency';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  public amount$: Observable<number>;
  public currencyRates$: Observable<Currency[]>;

  constructor(public store: Store<fromRoot.State>) {
    this.amount$ = store.select(fromRoot.getAmountState);
    this.currencyRates$ = store.select(fromRoot.getCurrencyRates);
  }

  /* Dispatching the action
      For our effect to be triggered, we have to dispatch a CurrencyUpdateAction.
      For simplicity, I've done so once, when the app.component is initialized.
      You probably want to update the value periodically in a real application.
  */

  ngOnInit() {
    this.store.dispatch(new CurrenciesUpdateAction());
  }

  onAmountChange(amount: string) {
    const num = parseFloat(amount);
    if (!isNaN(num)) {
      this.store.dispatch(new AmountChangeAction(num));
    }
  }
}

/*
Whenever the input's value changes, the specified method is called inside of our component.
All we need to do is to create that method. But first, we need to request the store instance in our constructor.

But that alone is not sufficient. We actually need a two-way binding to our store...

But before we can begin, we need to define some selectors.

NgRx selectors
Selectors are a concept of ngrx-store. They are just small and simple functions,
that allow us, to get only the part of the application state, that we are interested in.
I've created these functions in the index.ts inside of the reducers directory.

Now that we have the selectors in place, we can get our observables from the store.
For that, we can use the select() method.
note the import of Observable at the top of the code.

We have created to public variables amount$ & currencyRates$ which are both of the type Observable.
In our constructor, we then get an observable of the corresponding sub-state using the select() method.
To get updates from our store-state, we would subscribe to that observable or use an async pipe.
*/

