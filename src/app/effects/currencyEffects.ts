import { CurrenciesUpdatedAction } from './../actions/currency';
import { CurrencyService } from './../services/currency.service';
import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { Action } from '@ngrx/store';

import * as currency from '../actions/currency';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';

@Injectable()
export class CurrencyEffects {
  @Effect()
  update$: Observable<Action> =
    this.actions$.pipe(
    ofType(currency.CURRENCIESUPDATE),
    switchMap(() =>
      this.currencyService
      .getRates()
      .pipe( map(data => new CurrenciesUpdatedAction(data)) )
      )
    );

    constructor(
      private currencyService: CurrencyService,
      private actions$: Actions
    ) {}
}

/*
NOTE: THE TUTORIAL HAS TWO EXTRA IMPORTS THAT CAUSE THE COMPILER TO GENERATE ERRORS.
*/


/*
Dealing with side-effects (ngrx/effects)

As you may have noticed through this tutorial, that side-effects are considered "evil" in the ngrx ecosystem.
But sometimes, they can not be avoided.

The most common example of a side effect is a HTTP-request. We make it at some point,
but we don't know when it comes back. It is an asynchronous operation. To deal with this kind of side-effects,
the ngrx team has created a module called ngrx/effects.

The goal of this module is, to wrap the side-effects and isolate them from the rest of the code.

Creating an effect
Creating a ngrx effect is easy. All we need to do is to use the @Effect() decorator to decorate a method of an @Injectable.
For our example, we need an effect that downloads our currency-conversion-rates from an API.

For that, we are listening on the observable of all actions, until an action of the type CURRENCIESUPDATE comes along.
For each of these actions of the type CURRENCIESUPDATE we call currencyService and ask it for the current rates.

This service, that we will create later, then returns an observable itself.
Every value of that observable does then create a new CurrenciesUpdateAction containing the downloaded data
that is automatically dispatched to the store.
*/
