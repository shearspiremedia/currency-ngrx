import { HttpClient } from '@angular/common/http';
import { Currency } from './../models/currency';

import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { observable, Observable } from 'rxjs';
import { resultMemoize } from '@ngrx/store';

@Injectable()
export class CurrencyService {
  constructor(private http: HttpClient) {}

  getRates(): Observable<Currency[]> {
    return this.http
      .get<any>('https://api.exchangeratesapi.io/latest?base=USD')
      .pipe(
        map(result => {
          return Object.keys(result.rates).map((key, index) => {
            return {code: key, value: result.rates[key]}
          });
        })
      );
  }
}


/*
The currency service

That service is quite simple. All we do here is to call the API-Endpoint and return the response as an observable.
We also have to convert the answer from an object with key-value-pairs to an array of the type Currency.
We do so by using the map operator.
*/
