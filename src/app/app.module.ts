import { CurrencyService } from './services/currency.service';
import { CurrencyEffects } from './effects/currencyEffects';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers/index';

import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';

// npm install @ngrx/core @ngrx/effects @ngrx/store

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, StoreModule.forRoot(reducers), EffectsModule.forRoot([CurrencyEffects])
  ],
  providers: [CurrencyService],
  bootstrap: [AppComponent]
})
export class AppModule { }

/*
We import the store into our app-module.
Here we import the map of reducers and pass it as a parameter.
*/

/* Registering the effect
    Last but not least, we have to register our effect in the applications app.module.
    We also have to import the EffectsModule of ngrx.
    Also, don't forget to provide our CurrencyService at this point.
*/
