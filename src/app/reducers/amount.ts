import { ActionReducer, Action } from '@ngrx/store';
import * as amount from '../actions/amount';

export function reducer(state: number = 1, action: amount.AmountChangeAction) {
  switch (action.type){
    case amount.AMOUNTCHANGE:
      return action.payload;
    default:
      return state;
  }
}


/*
REDUCERS HAVE TO BE PURE FUNCTIONS.

Now that we have events in the form of actions, we need corresponding event listeners.
These listeners are called reducers. These reducers are the only structure that is allowed to alter the state.
All state changes are implemented inside of reducers.

After all, a reducer is just a function. It takes the current state and the action as parameters.
All we need to do is to create a function with that interface and register it at our store.
*/

/*
In this simple case, the sub-state amount is just a number.
Because of that, we can either return the new amount,
if the action is AMOUNTCHANGE or return the current state.
Notice that we import the previously created action here.
*/
