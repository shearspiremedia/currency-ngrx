import { Currency } from '../models/currency';

import * as fromAmount from './amount';
import * as fromCurrency from './currency';

export interface State {
  amount: number;
  currencies: Currency[];
}

export const reducers = {
  amount: fromAmount.reducer,
  currencies: fromCurrency.reducer
}
/*
Now we have two sub-states of our application. The amount and the currency-rates.
Next, we need to create one "Application State" out of this sub-states.
This is because our Redux store can only consist of one JavaScript object.

To define the application state, go ahead and create a new file called "index.ts" inside of the reducers folder.
Inside of that file, we first create the interface of our app's state.
*/

/*
For the store to be instantiated in our application, we need to register it at some point.
Doing so is quite easy. All we need to do is to import it into our app-module.

When registering the store, we also need to tell it about its reducers.
For that, it expects a map of all reducers, we want to register. So first, go ahead and create that map.
We can do so inside of the previously created index.ts. The map is in the form of a standard object.

The code seen in this comment is interleaved above.
import * as fromAmount from './amount'
import * as fromCurrency from './currency'

export const reducers = {
  amount: fromAmount.reducer,
  currencies: fromCurrency.reducer,
}
*/

/*
Whenever the input's value changes, the specified method is called inside of our component.
All we need to do is to create that method. But first, we need to request the store instance in our constructor.

But that alone is not sufficient. We actually need a two-way binding to our store...

But before we can begin, we need to define some selectors.

NgRx selectors
Selectors are a concept of ngrx-store. They are just small and simple functions,
that allow us, to get only the part of the application state, that we are interested in.
I've created these functions in the index.ts inside of the reducers directory.
*/

export const getAmountState = (state: State) => state.amount;
export const getCurrencyRates = (state: State) => state.currencies;
