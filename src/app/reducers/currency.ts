import { Currency } from './../models/currency';
import * as currency from '../actions/currency';

export function reducer(state = [], action: currency.CurrenciesUpdatedAction) {
  switch (action.type){
    case currency.CURRENCIESUPDATED:
      return action.payload;
    default:
      return state;
  }
}


/*
We also require a reducer for our currency-rates.
This reducer looks quite the same as the previous one.
The only difference is, that our sub-state is now an array of currency rates.
*/
