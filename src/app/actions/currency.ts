import { Currency } from './../models/currency';
import { Action } from '@ngrx/store';

export const CURRENCIESUPDATE = '[Currency] UpdateAll';
export const CURRENCIESUPDATED = '[Currency] UpdatedAll';

export class CurrenciesUpdateAction implements Action {
  type = CURRENCIESUPDATE;
}
export class CurrenciesUpdatedAction implements Action {
  type = CURRENCIESUPDATED;

  constructor(public payload: Currency[]) {}
}

/*
If you are familiar with events, actions are quite simple to understand.
Because they are just that. They report that something happened. Nothing more.
Actions do not describe how the state has to be changed.

Instead, they precisely describe what happened. Every action has a type.
The type is typically a string, that describes what happened. For example 'MouseDown'.

Actions can also carry an (optional) payload. That is because often times,
we need additional information, about the event. For example, we probably want to know,
where the mouse went down. This payload can be any JavaScript object.
*/

/*
We will also need two more actions.
The first one indicates that new currency data was requested.
The second one specifies, that new currency data has been downloaded.
*/
