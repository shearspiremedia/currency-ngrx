import { Action } from '@ngrx/store';

export const AMOUNTCHANGE = '[Amount] Change';

export class AmountChangeAction implements Action {
  type = AMOUNTCHANGE;
  constructor(public payload: number) {}
}

/*
If you are familiar with events, actions are quite simple to understand.
Because they are just that. They report that something happened. Nothing more.
Actions do not describe how the state has to be changed.

Instead, they precisely describe what happened. Every action has a type.
The type is typically a string, that describes what happened. For example 'MouseDown'.

Actions can also carry an (optional) payload. That is because often times,
we need additional information, about the event. For example, we probably want to know,
where the mouse went down. This payload can be any JavaScript object.
*/

/*
Our example application needs an input field, with the amount in dollar, that we want to convert to other currencies
This amount will be part of our applications state. Because of that, we will also need an action, to report,
that the amount was changed by the user.
*/
